Description
===========
The project is developed as a ECE 285 course project during Spring 2019 by team RAPR containing Parth Paritosh, Raghavendra Sivapuram, Rajat Sethi and Aditya Mishra.
We used the NYU Dataset (on the DSMLP server) for depth prediction using Convolutional Neural Networks. We used different architectures and loss functions in this investigation.
All the models have been learnt using scheduled learning rates and the number of epochs is around 30.

Requirements
============
Python packages: os, numpy, torch, torchvision, pandas, PIL, matplotlib, h5py, scipy

Code organization
=================
demo1.ipynb -- Run a demo of Network2 trained with Huber loss
demo2.ipynb -- Run a demo of Network2 trained with Scale Invariant loss
demo3.ipynb -- Run a demo of Network2 trained with Huber+Scale Invariant loss
network2-2.ipynb -- Run the training of Model 2 in literature
network3-3.ipynb -- Run the training of Model 3 in literature

The folders in the repository correspond to the checkpoints for the demos and training, and the .ipynbs load these checkpoints.

